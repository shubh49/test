from flask import Flask
from flask import render_template
import subprocess

app = Flask(__name__)

@app.route('/')
def hello_world():
	process = subprocess.Popen(['ls', '-a'], stdout=subprocess.PIPE)
	out, err = process.communicate()
	return out

@app.route('/html')
def static_page(name=None):
	return render_template('login.html', name=name)

if __name__ == '__main__':
	app.run()
